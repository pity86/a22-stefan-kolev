# Test Plan
The purpose of this document is to describe the testing types, shchedule, scope and exit criteria for the testing activities related to the forum's topic creation functionality of the Forum SK website.

## Testing types
* Functional Manual Black Box
    * Objective:
        Evaluate if the topics's creation functionality satisfies the functional requirements.

* Non-functional Automated Performance Stress
    * Objective:
        Evaluate the topic's creation functionality performance at and beyond the limit of the specified concurrent users.

* Positive and Negative
    * Objective:
    For both the Functional and Non-functional testing types positive and negative test will be created in order to test the topic's creation functionality in both ways for whcich it was inteneded and not intended to be used.

* Retesting and Regression
    * Objective:
    Retesting will be executed in order to verify the success of bug fixing for a particular defect.
    Regression testing will be executed in order to ensure that after bug fixing there are no other problems or side effects introduced to the unchanged area of the topic's creation functionality.


##  Schedule

| Testing Activity           | Start Date  | End Date    | Estimation    | Executed by
| ---------------------------| ------------| ----------- | ------------- |-----------
| Requirements Analysis      | 13-Sep-2020 | 13-Sep-2020 |  8 man-hours  | Test Тeam
| Test Planning              | 14-Sep-2020 | 14-Sep-2020 |  8 man-hours  | Test Manager/Test Analyst
| Test Development           | 15-Sep-2020 | 17-Sep-2020 | 24 man-hours  | Test Analyst/ Automation tester
| Test Enviorment Setup      | 18-Sep-2020 | 18-Sep-2020 |  8 man-hours  | DevOps/Test Analyst
| Test Execution             | 19-Sep-2020 | 20-Sep-2020 | 16 man-hours  | Automation tester/ Manual tester 
| Retesting and Regression   | 21-Sep-2020 | 21-Sep-2020 |  8 man-hours  | Automation tester/ Manual tester
| Test Reporting and Closure | 22-Sep-2020 | 22-Sep-2020 |  8 man-hours  | Test Manager/Test Analyst


## Scope of testing

### Functionalities to be tested
* Only authenticated user can create topic
* Only Latin script is allowed
* Maximum topic's length – 200 symbols
* The name of the creator appears under topic's name
* The exact time and date of creation appears under topic's name
* Existing topic's name not allowed to be created again
* Possibility to create up to 10 topics simultaneously


### Functionalities not to be tested
* Compatibility with mobile devices
* Word censoring

## Exit Criteria
* 100% tests executed
* 100% high priority tests passed
* 90% medium and low priority tests passed
* No blocker or critical defects found
* No more budget


