package com.telerikacademy.agency.models.vehicles.contracts;

import com.telerikacademy.agency.models.common.VehicleType;

public interface Airplane extends Vehicle {
    
    boolean hasFreeFood();

}
