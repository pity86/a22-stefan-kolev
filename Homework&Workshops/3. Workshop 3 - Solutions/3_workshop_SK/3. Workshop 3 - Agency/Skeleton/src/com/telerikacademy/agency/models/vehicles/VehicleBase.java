package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public abstract class VehicleBase implements Vehicle {

    private static final int MIN_PAS_VEH = 1;
    private static final int MAX_PAS_VEH = 800;
    private static final double MIN_PRICE_KM = 0.10;
    private static final double MAX_PRICE_KM = 2.50;
    private int passengerCapacity;
    private double pricePerKilometer;
    private VehicleType type;

    //finish the class

    public VehicleBase(int passengerCapacity, double pricePerKilometer, VehicleType type) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        setType(type);

    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    private void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < MIN_PAS_VEH || passengerCapacity > MAX_PAS_VEH) {
            throw new IllegalArgumentException(
            String.format("A vehicle with less than %d passengers or more than %d passengers cannot exist!",
                    MIN_PAS_VEH, MAX_PAS_VEH)
            );
    }
        this.passengerCapacity = passengerCapacity;
    }

    public double getPricePerKilometer() {
        return pricePerKilometer;
    }


    private void setPricePerKilometer(double pricePerKilometer) {
        if (pricePerKilometer< MIN_PRICE_KM || pricePerKilometer >MAX_PRICE_KM){
            throw new IllegalArgumentException(
                    String.format("A vehicle with a price per kilometer lower than $%.2f or higher than $%.2f cannot exist!",
                            MIN_PRICE_KM, MAX_PRICE_KM)
            );
        }
        this.pricePerKilometer = pricePerKilometer;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    private void setType(VehicleType type) {
        if(type == null){
            throw new IllegalArgumentException("Type can't be null");
        }
        this.type = type;
    }

    public abstract String printClassName();

    public String print() {
        return String.format(
                 "%s ----" + System.lineSeparator()+
                "Passenger capacity: %s" + System.lineSeparator() +
                        "Price per kilometer: %.2f" + System.lineSeparator() +
                        "Vehicle type: %s",printClassName(), getPassengerCapacity(), getPricePerKilometer(), getType());
    }

    @Override
    public String toString() {
        return print();
    }

}

