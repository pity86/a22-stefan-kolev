package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;


public class BusImpl extends VehicleBase implements Bus {

    private static final int MIN_PAS_BUS = 10;
    private static final int MAX_PAS_BUS = 50;

    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);

        if(passengerCapacity< MIN_PAS_BUS || passengerCapacity > MAX_PAS_BUS){
            throw new IllegalArgumentException(
                    String.format("A bus cannot have less than %d passengers or more than %d passengers.",
                            MIN_PAS_BUS, MAX_PAS_BUS)
            );
        }
    }

    @Override
    public String printClassName() {
        return "Bus";
    }

    @Override
    public String print() {
        return String.format("%s%n", super.print() );
    }


//    public String toString() {
//        return print();
//    }
}
