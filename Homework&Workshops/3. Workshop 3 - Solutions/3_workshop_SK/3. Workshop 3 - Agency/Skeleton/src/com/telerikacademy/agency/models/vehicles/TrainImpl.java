package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleBase implements Train {

    private static final int MIN_PAS_TRAIN = 30;
    private static final int MAX_PAS_TRAIN = 150;
    private static final int MIN_CART = 1;
    private static final int MAX_CART = 15;
    private int carts;

    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
        setCarts(carts);

        if (passengerCapacity < MIN_PAS_TRAIN || passengerCapacity > MAX_PAS_TRAIN) {
            throw new IllegalArgumentException("A train cannot have less than 30 passengers or more than 150 passengers.");
        }
    }

        @Override
        public int getCarts() {
            return carts;
        }

        private void setCarts (int carts){
            if (carts < MIN_CART || carts > MAX_CART) {
                throw new IllegalArgumentException("A train cannot have less than 1 cart or more than 15 carts.");
            }
            this.carts = carts;
        }

    @Override
    public String printClassName() {
        return "Train";
    }

    @Override
    public String print() {
        return String.format("%s%nCarts amount: %d%n", super.print(), getCarts());
    }

//    public String toString() {
//        return print();
//    }

}
