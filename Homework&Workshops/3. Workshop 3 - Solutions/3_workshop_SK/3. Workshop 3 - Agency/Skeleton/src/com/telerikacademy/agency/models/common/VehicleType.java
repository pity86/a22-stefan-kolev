package com.telerikacademy.agency.models.common;

public enum VehicleType {
    LAND,
    AIR,
    SEA;

    @Override
    public String toString() {
        return this.name().substring(0, 1) + this.name().substring(1).toLowerCase();

//    @Override
//    public String toString() {
//        switch (this) {
//            case LAND:
//                return "Land";
//            case AIR:
//                return "Air";
//            case SEA:
//                return "Sea";
//            default:
//                throw new IllegalArgumentException();
//        }
    }
}
