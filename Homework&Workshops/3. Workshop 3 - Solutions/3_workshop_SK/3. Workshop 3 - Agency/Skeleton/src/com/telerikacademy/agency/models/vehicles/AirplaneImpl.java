package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Airplane;

public class AirplaneImpl extends VehicleBase implements Airplane {

    private boolean hasFreeFood;

    public AirplaneImpl(int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        super(passengerCapacity, pricePerKilometer, VehicleType.AIR);
        setHasFreeFood(hasFreeFood);
    }

    @Override
    public boolean hasFreeFood() {
        return hasFreeFood;
    }

    public void setHasFreeFood(boolean hasFreeFood) {
        this.hasFreeFood = hasFreeFood;
    }

    @Override
    public String printClassName() {
        return "Airplane";
    }

    @Override
    public String print() {

        return String.format("%s%nHas free food: %s%n", super.print(), hasFreeFood());
    }
//    public String toString() {
//        return print();
//    }
}
