package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Airplane;

public class AirplaneImpl extends VehicleBase implements Airplane {

    private final boolean freeFood;

    public AirplaneImpl(int passengerCapacity, double pricePerKilometer, boolean freeFood) {
        super(passengerCapacity, pricePerKilometer, VehicleType.AIR);
        setType(VehicleType.AIR);
        this.freeFood = freeFood;
    }

    @Override
    public boolean hasFreeFood() {
        return freeFood;
    }

    @Override
    public String printClassName() {
        return "Airplane";
    }

    @Override
    public String print() {
        return String.format(super.print() + "Has free food: %s" + System.lineSeparator(),
                hasFreeFood());
    }
}
