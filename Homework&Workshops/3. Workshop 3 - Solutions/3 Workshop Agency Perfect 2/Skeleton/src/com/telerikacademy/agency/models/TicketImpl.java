package com.telerikacademy.agency.models;

import com.telerikacademy.agency.models.contracts.Journey;
import com.telerikacademy.agency.models.contracts.Ticket;

public class TicketImpl implements Ticket {

    private final Journey journey;
    private double administrativeCosts;

    public TicketImpl(Journey journey, double administrativeCosts) {
        this.journey = journey;
        setAdministrativeCosts(administrativeCosts);
    }

    public void setAdministrativeCosts(double administrativeCosts) {
        if(administrativeCosts < 0) throw new IllegalArgumentException();
        this.administrativeCosts = administrativeCosts;
    }

    @Override
    public double getAdministrativeCosts() {
        return administrativeCosts;
    }

    @Override
    public Journey getJourney() {
        return journey;
    }

    @Override
    public double calculatePrice() {
        return administrativeCosts * journey.calculateTravelCosts();
    }

    @Override
    public String print() {
        return String.format("Ticket ----" + System.lineSeparator() +
                "Destination: %s" + System.lineSeparator() +
                "Price: %.2f" + System.lineSeparator(),
                journey.getDestination(),
                calculatePrice());
    }

    @Override
    public String toString() {
        return print();
    }
}
