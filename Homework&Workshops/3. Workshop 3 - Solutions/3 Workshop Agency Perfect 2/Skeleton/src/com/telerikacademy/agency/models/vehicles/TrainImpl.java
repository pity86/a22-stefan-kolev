package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.helpers.Validator;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleBase implements Train {

    public static final int PASSENGERS_MIN = 30;
    public static final int PASSENGERS_MAX = 150;

    public static final int CARTS_MIN = 1;
    public static final int CARTS_MAX = 15;

    public static final String PASSENGERS_OUT_OF_RANGE = String.format("A train cannot have less than %d passengers or more than %d passengers.", PASSENGERS_MIN, PASSENGERS_MAX);
    public static final String CARTS_OUT_OF_RANGE = String.format("A train cannot have less than %d cart or more than %d carts.", CARTS_MIN, CARTS_MAX);

    private int carts;

    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
        setType(VehicleType.LAND);
        setCarts(carts);
    }

    public void setCarts(int carts) {
        Validator.inRange(carts, CARTS_MIN, CARTS_MAX, CARTS_OUT_OF_RANGE);
        this.carts = carts;
    }

    @Override
    public void setPassengerCapacity(int passengerCapacity) {
        Validator.inRange(passengerCapacity, PASSENGERS_MIN, PASSENGERS_MAX, PASSENGERS_OUT_OF_RANGE);
        super.setPassengerCapacity(passengerCapacity);
    }

    @Override
    public int getCarts() {
        return carts;
    }

    @Override
    public String printClassName() {
        return "Train";
    }

    @Override
    public String print() {
        return String.format(super.print() + "Carts amount: %d" + System.lineSeparator(),
                getCarts());
    }
}
