package com.telerikacademy.agency.models.helpers;

public class  Validator {

    public static void notNull(Object object) {
        if (object == null) throw new IllegalArgumentException();
    }

    public static void notNull(Object object, String message) {
        if (object == null) throw new IllegalArgumentException(message);
    }

    public static void inRange(double num, double min, double max, String message) {
        if (num < min || num > max) throw new IllegalArgumentException(message);
    }
}
