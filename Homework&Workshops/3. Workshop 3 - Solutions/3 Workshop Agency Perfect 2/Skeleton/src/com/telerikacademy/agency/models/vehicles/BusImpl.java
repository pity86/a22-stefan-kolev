package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.helpers.Validator;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleBase implements Bus {

    public static final int PASSENGERS_MIN = 10;
    public static final int PASSENGERS_MAX = 50;

    public static final String PASSENGERS_OUT_OF_RANGE = String.format("A bus cannot have less than %d passengers or more than %d passengers.", PASSENGERS_MIN, PASSENGERS_MAX);

    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
        setType(VehicleType.LAND);
    }

    @Override
    public void setPassengerCapacity(int passengerCapacity) {
        Validator.inRange(passengerCapacity, PASSENGERS_MIN, PASSENGERS_MAX, PASSENGERS_OUT_OF_RANGE);
        super.setPassengerCapacity(passengerCapacity);
    }

    @Override
    public String printClassName() {
        return "Bus";
    }
}
