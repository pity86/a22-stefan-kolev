package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.helpers.Validator;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public abstract class VehicleBase implements Vehicle {

    public static final String TYPE_CANT_BE_NULL = "Vehicle type is required";

    public static final int PASSENGERS_MIN = 1;
    public static final int PASSENGERS_MAX = 800;

    public static final double PRICE_MIN = 0.1;
    public static final double PRICE_MAX = 2.5;

    public static final String PASSENGERS_OUT_OF_RANGE = String.format("A vehicle with less than %d passengers or more than %d passengers cannot exist!", PASSENGERS_MIN, PASSENGERS_MAX);
    public static final String PRICE_OUT_OF_RANGE = String.format("A vehicle with a price per kilometer lower than $%.2f or higher than $%.2f cannot exist!", PRICE_MIN, PRICE_MAX);

    private int passengerCapacity;
    private double pricePerKilometer;
    private VehicleType type;
    
    public VehicleBase(int passengerCapacity, double pricePerKilometer, VehicleType type) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        Validator.notNull(type, TYPE_CANT_BE_NULL);
    }

    public void setPassengerCapacity(int passengerCapacity) {
        Validator.inRange(passengerCapacity, PASSENGERS_MIN, PASSENGERS_MAX, PASSENGERS_OUT_OF_RANGE);
        this.passengerCapacity = passengerCapacity;
    }

    public void setPricePerKilometer(double pricePerKilometer) {
        Validator.inRange(pricePerKilometer, PRICE_MIN, PRICE_MAX, PRICE_OUT_OF_RANGE);
        this.pricePerKilometer = pricePerKilometer;
    }

    public void setType(VehicleType type) {
        this.type = type;
    }

    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    public VehicleType getType() {
        return type;
    };

    @Override
    public String print() {
        return String.format("%s ----" + System.lineSeparator() +
                        "Passenger capacity: %d" + System.lineSeparator() +
                        "Price per kilometer: %.2f" + System.lineSeparator() +
                        "Vehicle type: %s" + System.lineSeparator(),
                printClassName(),
                getPassengerCapacity(),
                getPricePerKilometer(),
                getType());
    }

    public abstract String printClassName();

    @Override
    public String toString() {
        return print();
    }
}
