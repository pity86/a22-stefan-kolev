package com.telerikacademy.agency.models;

import com.telerikacademy.agency.models.contracts.Journey;
import com.telerikacademy.agency.models.contracts.Ticket;

public class TicketImpl implements Ticket {

    private Journey journey;
    private double administrativeCosts;

    public TicketImpl(Journey journey, double administrativeCosts) {
        this.journey = journey;
        this.administrativeCosts = administrativeCosts;
    }

    @Override
    public double getAdministrativeCosts() {
        return this.administrativeCosts;
    }

    @Override
    public Journey getJourney() {
        return this.journey;
    }

    @Override
    public double calculatePrice() {
        return getAdministrativeCosts() * getJourney().calculateTravelCosts();
    }

    @Override
    public String print() {
        return this.toString();
    }

    @Override
    public String toString() {
        return String.format(
                "Ticket ----\n" +
                        "Destination: %s\n" +
                        "Price: %.2f\n", getJourney().getDestination(), calculatePrice());
    }
}
