package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleBase implements Train {

    public static final int MIN_CARTS = 1;
    private static final int PASSENGER_CAPACITY_MIN = 30;
    private static final int PASSENGER_CAPACITY_MAX = 150;
    public static final int MAX_CARTS = 15;

    private int carts;

    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
        super.setPassengerCapacity(passengerCapacity, PASSENGER_CAPACITY_MIN, PASSENGER_CAPACITY_MAX);
        setCarts(carts);
    }

    @Override
    public int getCarts() {
        return this.carts;
    }

    @Override
    public String toString() {
        return String.format("Train ----\n" +
                super.print() + "\n" +
                "Carts amount: %s\n", getCarts());
    }

    private void setCarts(int carts) {
        if (carts < MIN_CARTS || carts > MAX_CARTS) {
            throw new IllegalArgumentException(String.format("A train cannot have less than %d cart or more than %d carts.",
                    MIN_CARTS, MAX_CARTS));
        }
        this.carts = carts;
    }
}
