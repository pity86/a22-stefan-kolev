package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public class VehicleBase implements Vehicle {

    private static final int PASSENGER_CAPACITY_MIN = 1;
    private static final int PASSENGER_CAPACITY_MAX = 800;
    public static final double MIN_PRICE_PER_KM = 0.10;
    public static final double MAX_PRICE_PER_KM = 2.50;
    private int passengerCapacity;
    private double pricePerKilometer;
    private VehicleType type;

    public VehicleBase(int passengerCapacity, double pricePerKilometer, VehicleType type) {
        setPassengerCapacity(passengerCapacity, PASSENGER_CAPACITY_MIN, PASSENGER_CAPACITY_MAX);
        setPricePerKilometer(pricePerKilometer);

        if (type == null) {
            throw new IllegalArgumentException(String.format("TYPE_CANT_BE_%s", this.type));
        }
        this.type = type;
    }

    @Override
    public int getPassengerCapacity() {
        return this.passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return this.pricePerKilometer;
    }

    @Override
    public VehicleType getType() {
        return this.type;
    }

    protected void setPassengerCapacity(int passengerCapacity, int min, int max) {
        if (passengerCapacity < min || passengerCapacity > max) {
            throw new IllegalArgumentException(String.format
                    ("A train cannot have less than %d passengers or more than %d passengers.",
                            min, max));
        }
        this.passengerCapacity = passengerCapacity;
    }

    public void setPricePerKilometer(double pricePerKilometer) {
        if (pricePerKilometer < MIN_PRICE_PER_KM || pricePerKilometer > MAX_PRICE_PER_KM) {
            throw new IllegalArgumentException(String.format
                    ("A vehicle with a price per kilometer lower than $%.2f or higher than $%.2f cannot exist!",
                            MIN_PRICE_PER_KM, MAX_PRICE_PER_KM));
        }
        this.pricePerKilometer = pricePerKilometer;
    }

    @Override
    public String print() {
        return String.format(
                "Passenger capacity: %d\n" +
                        "Price per kilometer: %.2f\n" +
                        "Vehicle type: %s", getPassengerCapacity(), getPricePerKilometer(), getType());
    }

    public String printClassName() {
        return null;
    }
}
