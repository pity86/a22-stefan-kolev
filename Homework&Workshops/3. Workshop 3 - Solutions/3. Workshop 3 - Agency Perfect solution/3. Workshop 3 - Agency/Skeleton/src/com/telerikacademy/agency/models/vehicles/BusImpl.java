package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleBase implements Bus {

    private static final int PASSENGER_CAPACITY_MIN = 10;
    private static final int PASSENGER_CAPACITY_MAX = 50;

    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
        super.setPassengerCapacity(passengerCapacity, PASSENGER_CAPACITY_MIN, PASSENGER_CAPACITY_MAX);
    }

    @Override
    public String toString() {
        return String.format("Bus ----\n" +
                super.print() + "\n"
        );
    }
}
