package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public abstract class VehicleBase implements Vehicle {

    public static final int MIN_CAPACITY = 1;
    public static final int MAX_CAPACITY = 800;
    public static final double MIN_PRICE = 0.10;
    public static final double MAX_PRICE = 2.5;
    private static final String TYPE_CANT_BE_NULL = "Type cannot be null";
    private int passengerCapacity;
    private double pricePerKilometer;
    private VehicleType type;
    
    public VehicleBase(int passengerCapacity, double pricePerKilometer, VehicleType type) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        if (type == null) {
            throw new IllegalArgumentException(TYPE_CANT_BE_NULL);
        }
        this.type = type;
    }


    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    private void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < getMinCapacity() || passengerCapacity > getMaxCapacity()) {
//            boolean isNull = printClassName() == null;
//            if (isNull) {
//                throw new IllegalArgumentException(
//                        String.format("A %s with less than %d passengers or more than %d passengers cannot exist!",
//                                "vehicle",
//                                getMinCapacity(),
//                                getMaxCapacity())
//                );
//            } else {
//                throw new IllegalArgumentException(
//                        String.format("A %s with less than %d passengers or more than %d passengers cannot exist!",
//                                printClassName().toLowerCase(),
//                                getMinCapacity(),
//                                getMaxCapacity())
//                );
//            }

            throw new IllegalArgumentException(
                    String.format("A %s with less than %d passengers or more than %d passengers cannot exist!",
                            printClassName() == null ? "vehicle": printClassName().toLowerCase(),
                            getMinCapacity(),
                            getMaxCapacity())
                    );
        }

        this.passengerCapacity = passengerCapacity;
    }

    protected int getMinCapacity() {
        return MIN_CAPACITY;
    }

    protected int getMaxCapacity() {
        return MAX_CAPACITY;
    }

    private void setPricePerKilometer(double pricePerKilometer) {
        if (pricePerKilometer < MIN_PRICE || pricePerKilometer > MAX_PRICE) {
            throw new IllegalArgumentException(
                    "A vehicle with a price per kilometer lower than $0.10 or higher than $2.50 cannot exist!");
        }
        this.pricePerKilometer = pricePerKilometer;
    }

    @Override
    public String print() {
        return String.format("%s ----\n" +
                "Passenger capacity: %d\n" +
                "Price per kilometer: %.2f\n" +
                "Vehicle type: %s",
                printClassName(),
                getPassengerCapacity(),
                getPricePerKilometer(),
                getType());
    }

    @Override
    public String toString() {
        return print();
    }

    protected abstract String printClassName();

}
