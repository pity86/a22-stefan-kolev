package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleBase implements Train {

    public static final int MIN_CARTS = 1;
    public static final int MAX_CARTS = 15;
    public static final String TRAIN = "Train";
    public static final int MAX_CAPACITY = 150;
    public static final int MIN_CAPACITY = 30;
    public static final String VALIDATE_CARTS_ERROR_MESSAGE =
            "A train cannot have less than %d cart or more than %d carts.";
    private int carts;

    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
        setCarts(carts);
    }

    @Override
    protected String printClassName() {
        return TRAIN;
    }

    @Override
    public int getCarts() {
        return carts;
    }

    public void setCarts(int carts) {
        if (carts < MIN_CARTS || carts > MAX_CARTS) {
            //TODO format message!
            throw new IllegalArgumentException(
                    String.format(VALIDATE_CARTS_ERROR_MESSAGE,
                            MIN_CARTS,
                            MAX_CARTS)
                    );
        }
        this.carts = carts;
    }

    @Override
    public String print() {
        return String.format("%s%n" +
                        "Carts amount: %d",
                super.print(),
                getCarts());
    }

    @Override
    protected int getMinCapacity() {
        return MIN_CAPACITY;
    }

    @Override
    protected int getMaxCapacity() {
        return MAX_CAPACITY;
    }
}
