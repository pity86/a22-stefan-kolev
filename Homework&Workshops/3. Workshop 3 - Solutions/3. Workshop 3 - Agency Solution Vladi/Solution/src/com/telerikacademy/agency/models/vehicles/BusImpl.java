package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleBase implements Bus {

    public static final String BUS = "Bus";
    public static final int MIN_CAPACITY = 10;
    public static final int MAX_CAPACITY = 50;

    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
    }

    @Override
    protected String printClassName() {
        return BUS;
    }

    @Override
    protected int getMinCapacity() {
        return MIN_CAPACITY;
    }

    @Override
    protected int getMaxCapacity() {
        return MAX_CAPACITY;
    }
}
