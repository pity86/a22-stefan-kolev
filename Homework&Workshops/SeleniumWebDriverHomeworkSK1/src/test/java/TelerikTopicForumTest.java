import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class TelerikTopicForumTest {

    public static final String TELERIKACADEMY_COM = "https://stage-forum.telerikacademy.com/";
    public static final int TIME = 2000;
    public static final String result = "Create new Topic from Home Page";
    public static final String alphaPrep = "Alpha Preparation";
    public static final String prep = "preparation";
    public static final String resultatTen = "Post must be at least 10 characters";
    public static final String addCategory = "Add category and tag to new topic";
    public static final String createTopicLessSymbols = "Create topic with less than minimum required symbols in post";
    public static final String create = "Create";
    public static final String emailat = "stefan.kolev.a22@learn.telerikacademy.com";
    public static final String pass = "???";
    private WebDriver driver;

    @BeforeClass
    public static void ClassInit() {
        System.setProperty("webdriver.chrome.driver", "D:\\SeleniumWebChromeGeckoDrivers\\chromedriver.exe");
    }

    @Before
    public void TestInit() {
        driver = new ChromeDriver();
    }

    @Test
    public void CreateTopicHomePage() {
        driver.get(TELERIKACADEMY_COM);
        LoginForum(driver);

        WebElement buttonNewTopic = driver.findElement(By.xpath("//*[@id='create-topic']/span"));
        buttonNewTopic.click();

        waitUntilTimeout(TIME);

        WebElement topicName = driver.findElement(By.xpath("//input[@id='reply-title']"));
        topicName.sendKeys(result);

        waitUntilTimeout(TIME);

        WebElement postTopic = driver.findElement(By.xpath("//textarea[@tabindex='4']"));
        postTopic.sendKeys(result);

        waitUntilTimeout(TIME);

        WebElement buttonCreateTopic = driver.findElement(By.xpath("//button[@tabindex='5']"));
        buttonCreateTopic.click();

        waitUntilTimeout(TIME);

        Assert.assertTrue(driver.getPageSource().contains(result));

        String bodyText = driver.findElement(By.xpath("//a[@class='fancy-title']")).getText();
        Assert.assertTrue(bodyText.contains(result));
    }

    @Test
    public void AddCategoryAndTagToNewTopic() {
        driver.get(TELERIKACADEMY_COM);
        LoginForum(driver);

        WebElement buttonNewTopic = driver.findElement(By.xpath("//*[@id='create-topic']/span"));
        buttonNewTopic.click();

        waitUntilTimeout(TIME);

        WebElement topicName = driver.findElement(By.xpath("//input[@id='reply-title']"));
        topicName.sendKeys(addCategory);

        waitUntilTimeout(TIME);

        WebElement postTopic = driver.findElement(By.xpath("//textarea[@tabindex='4']"));
        postTopic.sendKeys(addCategory);

        waitUntilTimeout(TIME);

        WebElement category = driver.findElement(By.xpath("//div[@tabindex='3']"));
        category.click();

        WebElement alphaPreparation = driver.findElement(By.xpath("//li[@data-name='Alpha Preparation']"));
        alphaPreparation.click();

        waitUntilTimeout(TIME);

        WebElement tags = driver.findElement(By.xpath("//div[@category-id='15']"));
        tags.click();

        waitUntilTimeout(TIME);

        WebElement preparation = driver.findElement(By.xpath("//li[@data-name='preparation']"));
        preparation.click();

        waitUntilTimeout(TIME);

        WebElement buttonCreateTopic = driver.findElement(By.xpath("//button[@tabindex='5']"));
        buttonCreateTopic.click();

        waitUntilTimeout(TIME);

        Assert.assertTrue(driver.getPageSource().contains(alphaPrep));
        Assert.assertTrue(driver.getPageSource().contains(prep));

        String tagText = driver.findElement(By.xpath("//div[@class='topic-header-extra']")).getText();
        Assert.assertTrue(tagText.contains(prep));
    }

    @Test
    public void CreateTopicWithLessThanMinimumRequiredSymbolsInPost() {
        driver.get(TELERIKACADEMY_COM);
        LoginForum(driver);

        WebElement buttonNewTopic = driver.findElement(By.xpath("//*[@id='create-topic']/span"));
        buttonNewTopic.click();

        waitUntilTimeout(TIME);

        WebElement topicName = driver.findElement(By.xpath("//input[@id='reply-title']"));
        topicName.sendKeys(createTopicLessSymbols);

        waitUntilTimeout(TIME);

        WebElement postTopic = driver.findElement(By.xpath("//textarea[@tabindex='4']"));
        postTopic.sendKeys(create);

        waitUntilTimeout(TIME);

        WebElement buttonCreateTopic = driver.findElement(By.xpath("//button[@tabindex='5']"));
        buttonCreateTopic.click();

        waitUntilTimeout(TIME);

        Assert.assertTrue(driver.getPageSource().contains(resultatTen));
    }

    private void LoginForum(WebDriver driver) {
        WebElement login = driver.findElement(By.xpath("//span[contains(text(), 'Log In')]"));
        waitUntilTimeout(TIME);
        login.click();

        WebElement signInTelerik = driver.findElement(By.xpath("//a[@id='TelerikAcademyAD']"));
        signInTelerik.click();

        waitUntilTimeout(TIME);

        WebElement email = driver.findElement(By.xpath("//input[@name='loginfmt']"));
        email.sendKeys(emailat);
        WebElement buttonNextEmail = driver.findElement(By.xpath("//input[@id='idSIButton9']"));
        buttonNextEmail.click();

        waitUntilTimeout(TIME);

        WebElement password = driver.findElement(By.xpath("//input[@name='passwd']"));
        password.sendKeys(pass);
        WebElement buttonSignIn = driver.findElement(By.xpath("//input[@id='idSIButton9']"));
        buttonSignIn.click();

        waitUntilTimeout(TIME);

        WebElement buttonNo = driver.findElement(By.xpath("//input[@id='idBtn_Back']"));
        buttonNo.click();

        waitUntilTimeout(TIME);
    }

    @After
    public void Closing() {
        driver.close();
    }

    private void waitUntilTimeout(long timeoutMilliseconds) {
        try {
            Thread.sleep(timeoutMilliseconds);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }
}


