# Homework:  Test cases covering the topic creation functionality of a forum: 	
  * Creation of topics 	

URL to test: https://stage-forum.telerikacademy.com/

# TC01
Test Title: Create new Topic from Home Page

Priority: Medium

Precondition: In Chrome webBrowser url https://stage-forum.telerikacademy.com/ loaded, user is logged in

Narrative:
1. In order to serve users
2. As a Forum owner
3. I want to allow users to create their own topics

Steps to execute:
1. Click on "New Topic" button
2. Enter title in field ('Create new Topic from Home Page')
3. Enter message in text area ('Create new Topic from Home Page')
4. Select and Click on "Create Topic" button 

Expected result: 
1. New topic appears. 
-----------------

## TC02
Test Title: Create topic with less than minimum requiried symbols in post - negative

Priority: Low

Precondition: In Chrome webBrowser url https://stage-forum.telerikacademy.com/ loaded, user is logged in

Narrative:
1. In order to serve users
2. As a Forum owner
3. I want users to write short post while creating their own topics

Steps to execute:
1. Click on "New Topic" button
2. Enter title in field ('Create topic with less than minimum requiried symbols in post')
3. Enter short message (6 symbols) in text area ('Create')
4. Select and Click on "Create Topic" button 

Expected result: 
1. A system message "Post must be at least 10 characters" appears. 
-----------------

## TC03
Test Title: Add category and tag to new topic

Priority: Low

Precondition: In Chrome webBrowser url https://stage-forum.telerikacademy.com/ loaded, user is logged in

Narrative:
1. In order to serve users
2. As a Forum owner
3. I want to allow users to add category while creating their own topics

Steps to execute:
1. Click on "New Topic" button
2. Enter title in field ('Add category and tag to new topic')
3. Enter message in text area ('Add category and tag to new topic')
4. Switch the drop down menu for category from "Uncategorized" to "Alpha Preparation".
5. Choose tag from tag field. (Data: preparation)
5. Select and Click on "Create Topic" button 

Expected result: 
1. New topic with Category and tag appears. 
-----------------
