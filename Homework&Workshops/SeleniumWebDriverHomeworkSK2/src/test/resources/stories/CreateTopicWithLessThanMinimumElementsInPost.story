Meta:
@forumLessTopic

Narrative:
As a forum user
I want to create topic with less elements
So that I can prove it will not accept

Scenario: Load chrome browser
Given Click login.Button element
And Wait for login.EmailRed
And Type pity86@abv.bg in login.EmailRed field
And Type ??? in login.PasswordRed field
And Click login.ButtonSignInRed element
And Wait for create.Topic
And Click create.Topic element
And Type Create topic with less than minimum required symbols in post in create.Less field
And Type Create in post.Topic field
Then Click button.Create element
