package testCases;

import pages.GooglePage;
import pages.TelerikAcademyPage;
import org.junit.Test;

public class QaAcademyNavigation extends BaseTest {
    String searchCriterion = "Telerik Academy";

    @Test
    public void navigateToCourseFromGoogleSearch() {
        GooglePage google = new GooglePage(actions.getDriver());

//Added agreeWithConsent method

        google.agreeWithConsent();

        TelerikAcademyPage academy = new TelerikAcademyPage(actions.getDriver());

        google.SearchAndOpenFirstResult(searchCriterion);
        academy.NavigateToQACourseViaCard();

        academy.AssertQAAcademySignupPageNavigated();
    }
}
