package testCases;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.GooglePage;

public class SearchInGoogleTest extends BaseTest {
    String searchCriterion = "Telerik Academy";

    @Test
    public void simpleGoogleSearch() {

//		Added agreeWithConsent method

        GooglePage google = new GooglePage(actions.getDriver());
        google.agreeWithConsent();
        google.SearchAndOpenFirstResult(searchCriterion);

        navigateToQACourseViaCard();

        actions.assertNavigatedUrl("academy.QASignUpUrl");
    }

    private void navigateToQACourseViaCard() {
        actions.clickElement("academy.AlphaAnchor");
        actions.waitForElementVisible("academy.Bottom", 10);
        actions.clickElement("academy.Bottom");
        actions.waitForElementVisible("academy.QaGetReadyLink", 10);
        actions.clickElement("academy.QaGetReadyLink");
        actions.clickElement("academy.SignUpNavButton");
    }
}
