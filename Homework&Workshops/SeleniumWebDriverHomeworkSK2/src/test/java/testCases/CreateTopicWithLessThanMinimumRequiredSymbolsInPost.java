package testCases;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.GooglePage;
import pages.TelerikForumPage;

public class CreateTopicWithLessThanMinimumRequiredSymbolsInPost extends BaseTest {

    String searchPage = "Stage forum telerik academy alpha";

    public static final String createTopicLessSymbols = "Create topic with less than minimum required symbols in post";
    public static final String create = "Create";
    public static final String resultatTen = "Post must be at least 10 characters";

    WebDriver driver;

    @Test
    public void CreateTopicWithLessThanMinimumRequiredSymbolsInPostTEST01() {

        GooglePage google = new GooglePage(actions.getDriver());
        google.agreeWithConsent();
        actions.typeValueInField(searchPage, "search.Input");
        actions.waitForElementVisible("search.Button", 10);
        actions.clickElement("search.Button");
        actions.waitForElementVisible("search.Stage", 10);
        actions.clickElement("search.Stage");

        TelerikForumPage forum = new TelerikForumPage(actions.getDriver());
        forum.LoginForum();


        actions.waitForElementVisible("create.Topic", 10);
        actions.clickElement("create.Topic");

        actions.typeValueInField(createTopicLessSymbols, "create.Less");

        actions.typeValueInField(create, "post.Topic");

        actions.waitForElementVisible("button.Create", 10);
        actions.clickElement("button.Create");

        actions.waitForElementVisible("create.Popup", 10);

        Assert.assertTrue(driver.getPageSource().contains(resultatTen));

    }
}







