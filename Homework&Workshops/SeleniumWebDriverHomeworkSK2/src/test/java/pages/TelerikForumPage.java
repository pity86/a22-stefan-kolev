package pages;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

public class TelerikForumPage extends BasePage {
    final String PAGE_URL = Utils.getConfigPropertyByKey("academy.forumUrl");

    public TelerikForumPage(WebDriver driver) {
        super(driver);
        super.setUrl(PAGE_URL);
    }

    public final String emailat = "pity86@abv.bg";
    public final String pass = "???";

    public void LoginForum() {

        actions.waitForElementVisible("login.Button", 10);
        actions.clickElement("login.Button");

        actions.waitForElementVisible("login.EmailRed", 10);
        actions.typeValueInField(emailat, "login.EmailRed");

        actions.typeValueInField(pass, "login.PasswordRed");

        actions.clickElement("login.ButtonSignInRed");

    }
}
