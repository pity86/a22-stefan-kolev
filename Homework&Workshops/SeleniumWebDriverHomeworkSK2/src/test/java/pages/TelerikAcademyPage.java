package pages;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.WebDriver;

public class TelerikAcademyPage extends BasePage {
    final String PAGE_URL = Utils.getConfigPropertyByKey("academy.url");

    public TelerikAcademyPage(WebDriver driver) {
        super(driver);
        super.setUrl(PAGE_URL);
    }

    public void NavigateToQACourseViaCard() {
        actions.clickElement("academy.AlphaAnchor");
        actions.clickElement("academy.Bottom");
        actions.waitForElementVisible("academy.QaGetReadyLink", 10);
        actions.clickElement("academy.QaGetReadyLink");
        actions.clickElement("academy.SignUpNavButton");
    }

    public void AssertQAAcademySignupPageNavigated() {
        actions.assertNavigatedUrl("academy.QASignUpUrl");
    }
}