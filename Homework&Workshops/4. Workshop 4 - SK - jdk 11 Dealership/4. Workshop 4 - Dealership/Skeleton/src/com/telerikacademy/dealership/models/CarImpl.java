package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Car;
import com.telerikacademy.dealership.models.contracts.Comment;


public class CarImpl extends VehicleBase implements Car {

//    public static final int MIN_SEATS = 1;
//    public static final int MAX_SEATS = 10;
    //look in DealershipFactoryImpl - use it to create proper constructor

    private int seats;

    public CarImpl(String make, String model, double price, int seats) {
        super(make, model, price, 4, VehicleType.CAR);
        setSeats(seats);

    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        if(seats == 0){
            throw new NullPointerException("seats cannot be 0");
        }
        if(seats < ModelsConstants.MIN_SEATS || seats > ModelsConstants.MAX_SEATS) {

            throw new IllegalArgumentException(String.format("Seats must be between %d and %d!", ModelsConstants.MIN_SEATS, ModelsConstants.MAX_SEATS));
        }

        this.seats = seats;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  Seats: %d", getSeats());
    }

    @Override
    public VehicleType getType() {
        return VehicleType.CAR;
    }

//    @Override
//    public void removeComment(Comment comment) {
//
//    }
//
//    @Override
//    public void addComment(Comment comment) {
//
//    }
}
