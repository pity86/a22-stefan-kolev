package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Motorcycle;

import javax.xml.validation.Validator;

public class MotorcycleImpl extends VehicleBase implements Motorcycle {

//    public static final int MIN_CATEGORY_LENGTH = 3;
//    public static final int MAX_CATEGORY_LENGTH = 10;

    private String category;

    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, price,2, VehicleType.MOTORCYCLE);
        setCategory(category);
    }

    @Override
    public String getCategory() {
        return category;
    }

    private void setCategory(String category) {
        if(category == null){
            throw new NullPointerException("category cannot be null");
        }
        if (category.length()< ModelsConstants.MIN_CATEGORY_LENGTH|| category.length()> ModelsConstants.MAX_CATEGORY_LENGTH){
            throw new IllegalArgumentException(String.format("Category must be between %d and %d characters long!", ModelsConstants.MIN_CATEGORY_LENGTH, ModelsConstants.MAX_CATEGORY_LENGTH));
        }
        this.category = category;
    }

    @Override

    protected String printAdditionalInfo() {
        return String.format("  Category: %s", getCategory());
    }

    @Override
    public VehicleType getType() {
        return VehicleType.MOTORCYCLE;
    }

//    @Override
//    public void removeComment(Comment comment) {
//
//    }
//
//    @Override
//    public void addComment(Comment comment) {
//
//    }

    //look in DealershipFactoryImpl - use it to create proper constructor
}
