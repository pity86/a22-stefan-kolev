package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Utils;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Motorcycle;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public abstract class VehicleBase implements Vehicle {

    private final static String MAKE_FIELD = "Make";
    private final static String MODEL_FIELD = "Model";
    private final static String PRICE_FIELD = "Price";
    private final static String WHEELS_FIELD = "Wheels";
    private final static String COMMENTS_HEADER = "    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER = "    --NO COMMENTS--";
    public static final int MOTOR_WHEELS = 2;
    public static final int CAR_WHEELS = 4;
    public static final int TRUCK_WHEELS = 8;
//    public static final int MIN_MODEL_LENGTH = 1;
//    public static final int MAX_MAKE_LENGTH = 15;
//    public static final int MAX_MODEL_LENGTH = 15;
//    public static final int MIN_MAKE_LENGTH = 2;
//    public static final double MIN_PRICE = 0.0;
//    public static final double MAX_PRICE = 1000000.0;

    //add fields
    private List<Comment> comments;
    private String make;
    private String model;
    private double price;
    private int wheels;
    private VehicleType vehicleType;

    public VehicleBase(String make, String model, double price, int wheels, VehicleType vehicleType) {
        comments = new ArrayList<>();
        setMake(make);
        setModel(model);
        setPrice(price);
        setWheels(wheels);
        setVehicleType(vehicleType);
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    public void addComment(Comment comment) {

        if (comment == null) {
            throw new IllegalArgumentException("Not acceptable");
        }
        comments.add(comment);
    }


    public void removeComment(Comment comment) {

        if (comment == null) {
            throw new IllegalArgumentException("Not acceptable");
        }
        comments.remove(comment);
    }


    @Override
    public String getMake() {
        return make;
    }

    private void setMake(String make) {
        if(make == null){
            throw new NullPointerException(String.format("%s cannot be null", MAKE_FIELD));
        }
        if (make.length() < ModelsConstants.MIN_MAKE_LENGTH || make.length() > ModelsConstants.MAX_MAKE_LENGTH){
            throw new IllegalArgumentException (String.format("%s must be between %d and %d characters long!", MAKE_FIELD, ModelsConstants.MIN_MAKE_LENGTH, ModelsConstants.MAX_MAKE_LENGTH));
        }
        this.make = make;

    }

    @Override
    public String getModel() {
        return model;
    }

    private void setModel(String model) {
        if(model == null){
            throw new NullPointerException(String.format("%s cannot be null", MODEL_FIELD));
        }
        if(model.length()< ModelsConstants.MIN_MODEL_LENGTH || model.length()> ModelsConstants.MAX_MODEL_LENGTH){
            throw new IllegalArgumentException(String.format("%s must be between %d and %d characters long!",MODEL_FIELD, ModelsConstants.MIN_MODEL_LENGTH, ModelsConstants.MAX_MODEL_LENGTH));
        }
        this.model = model;
    }

    @Override
    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        if(price < ModelsConstants.MIN_PRICE || price > ModelsConstants.MAX_PRICE) {
            throw new IllegalArgumentException( String.format("%s must be between %.1f and %.1f!",PRICE_FIELD, ModelsConstants.MIN_PRICE, ModelsConstants.MAX_PRICE ));
        }
        this.price = price;
    }

    @Override
    public int getWheels() {
        return wheels;
    }

    public void setWheels(int wheels) {
             if(wheels != MOTOR_WHEELS && wheels != CAR_WHEELS && wheels != TRUCK_WHEELS){
            throw new IllegalArgumentException(String.format("%s cannot be this number", WHEELS_FIELD));
        }
        this.wheels = wheels;
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    private void setVehicleType(VehicleType vehicleType) {
        if(vehicleType == null){
            throw new NullPointerException("vehicleType cannot be null");
        }
        this.vehicleType = vehicleType;
    }

//     finish the constructor and validate input; look in package com.telerikacademy.dealership.models.common.enums; what methods are there in VehicleType?
//    VehicleBase(String make, String model, double price, VehicleType vehicleType) {
//
//    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        
        builder.append(String.format("%s:", this.getClass().getSimpleName().replace("Impl", ""))).append(System.lineSeparator());

        //finish implementation of toString() - what should the next 3 lines of code append to the builder?


        builder.append(String.format("  %s: %s",MAKE_FIELD, getMake())).append(System.lineSeparator());
        builder.append(String.format("  %s: %s",MODEL_FIELD, getModel())).append(System.lineSeparator());
        builder.append(String.format("  %s: %d", WHEELS_FIELD, getWheels())).append(System.lineSeparator());
        
        builder.append(String.format("  %s: $%s", PRICE_FIELD, Utils.removeTrailingZerosFromDouble(price))).append(System.lineSeparator());
        
        if (!printAdditionalInfo().isEmpty()) {
            builder.append(printAdditionalInfo()).append(System.lineSeparator());
        }
        builder.append(printComments());
        return builder.toString();
    }
    
    //todo replace this comment with explanation why this method is protected:
    // Because we need to use it from all classes that have implemented it.
    protected abstract String printAdditionalInfo();
    
    private String printComments() {
        StringBuilder builder = new StringBuilder();
        
        if (comments.size() <= 0) {
            builder.append(String.format("%s", NO_COMMENTS_HEADER));
        } else {
            builder.append(String.format("%s", COMMENTS_HEADER)).append(System.lineSeparator());
            
            for (Comment comment : comments) {
                builder.append(comment.toString());
            }
            
            builder.append(String.format("%s", COMMENTS_HEADER));
        }
        
        return builder.toString();
    }
    
}
