package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Truck;

public class TruckImpl extends VehicleBase implements Truck {

//    public static final int MIN_CAPACITY = 1;
//    public static final int MAX_CAPACITY = 100;


    private int weightCapacity;

    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(make, model, price, 8, VehicleType.TRUCK);
       setWeightCapacity(weightCapacity);
    }

    @Override
    public int getWeightCapacity() {
        return weightCapacity;
    }

    private void setWeightCapacity(int weightCapacity) {
        if(weightCapacity == 0){
            throw new NullPointerException("weightCapacity cannot be 0");
        }
        if(weightCapacity < ModelsConstants.MIN_CAPACITY || weightCapacity > ModelsConstants.MAX_CAPACITY) {
            throw new IllegalArgumentException(String.format("Weight capacity must be between %d and %d!", ModelsConstants.MIN_CAPACITY, ModelsConstants.MAX_CAPACITY ));
        }
        this.weightCapacity = weightCapacity;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  Weight Capacity: %dt", getWeightCapacity() );
    }

    @Override
    public VehicleType getType() {
        return VehicleType.TRUCK;
    }

//    @Override
//    public void removeComment(Comment comment) {
//
//    }
//
//    @Override
//    public void addComment(Comment comment) {
//
//    }

    //look in DealershipFactoryImpl - use it to create proper constructor
    
}
