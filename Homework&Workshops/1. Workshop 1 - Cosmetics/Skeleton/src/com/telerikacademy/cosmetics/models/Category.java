package com.telerikacademy.cosmetics.models;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.products.Product;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

public class Category {
    
    private String name;
    private List<Product> products;
    
    public Category(String name) {
        if (name.length() < 2 || name.length() > 15){
            throw new IllegalArgumentException();
        }
        this.name = name;
        products = new ArrayList<>();
    }

    public List<Product> getProducts() {
        return this.products;
        
    }
    
    public void addProduct(Product product) {
        if (product == null) {
            throw new IllegalArgumentException();
        }

        this.products.add(product);
    }

    public void removeProduct(Product product) {
        if(product == null){
            throw new IllegalArgumentException();
        }
        if(!this.products.contains(product)){
            throw new IllegalArgumentException();
        }
        this.products.remove(product);
    }
    
    public String print() {
        return "";
    }
    
}
