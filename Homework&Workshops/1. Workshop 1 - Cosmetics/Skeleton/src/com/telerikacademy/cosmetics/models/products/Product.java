package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class Product {
    
    private double price;
    private String name;
    private String brand;
    private GenderType gender;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        if (price<0) {
            throw new IllegalArgumentException();
        }
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.length() < 3 || name.length() > 10) {
            throw new IllegalArgumentException();
        }
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        if (brand.length() < 2 || brand.length() > 10) {
            throw new IllegalArgumentException();
        }
        this.brand = brand;
    }

    public GenderType getGender() {

        return gender;
    }

    public void setGender(GenderType gender) {
        if (gender != GenderType.MEN && gender != GenderType.WOMAN && gender != GenderType.UNISEX) {
            throw new IllegalArgumentException();
        }
        this.gender = gender;
    }

    public Product(String name, String brand, double price, GenderType gender){

        setName(name);
        setBrand(brand);
        setPrice(price);
        setGender(gender);


    }
    public String print() {
        return "";
        // Format:
        //" #[Name] [Brand]
        // #Price: [Price]
        // #Gender: [Gender]
        // ==="
        
    }
    
}
