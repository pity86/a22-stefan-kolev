package com.telerikacademy.cosmetics.models.cart;

import com.telerikacademy.cosmetics.models.products.Product;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {

    private List<Product> productList;

    public ShoppingCart() {

        productList = new ArrayList<>();
    }

    public List<Product> getProductList() {
        return new ArrayList<>(productList);
    }

    public void addProduct(Product product) {
        if (product == null) {
            throw new IllegalArgumentException();
        }
        this.productList.add(product);
    }

    public void removeProduct(Product product) {
        if (product == null) {
            throw new IllegalArgumentException();
        }
        this.productList.remove(product);
    }

    public boolean containsProduct(Product product) {
        if (product == null) {
            throw new IllegalArgumentException();
        }
        return this.productList.contains(product);
    }

    public double totalPrice() {
        throw new NotImplementedException();
    }
    
}
