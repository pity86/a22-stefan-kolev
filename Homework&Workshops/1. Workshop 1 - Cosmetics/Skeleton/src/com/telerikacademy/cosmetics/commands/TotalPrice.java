package com.telerikacademy.cosmetics.commands;

import com.telerikacademy.cosmetics.commands.contracts.Command;
import com.telerikacademy.cosmetics.core.contracts.CosmeticsFactory;
import com.telerikacademy.cosmetics.core.contracts.CosmeticsRepository;

import java.util.List;

import static com.telerikacademy.cosmetics.commands.CommandConstants.TOTAL_PRICE_IN_SHOPPING_CART;

public class TotalPrice implements Command {
    
    private CosmeticsRepository cosmeticsRepository;
    private CosmeticsFactory cosmeticsFactory;
    
    public TotalPrice(CosmeticsFactory cosmeticsFactory, CosmeticsRepository cosmeticsRepository) {
        this.cosmeticsFactory = cosmeticsFactory;
        this.cosmeticsRepository = cosmeticsRepository;
    }
    
    @Override
    public String execute(List<String> parameters) {
        return cosmeticsRepository.getShoppingCart().getProductList() != null && cosmeticsRepository.getShoppingCart().getProductList().size() > 0 ?
                String.format(TOTAL_PRICE_IN_SHOPPING_CART, cosmeticsRepository.getShoppingCart().totalPrice()) :
                "No product in shopping cart!";
    }
    
}
