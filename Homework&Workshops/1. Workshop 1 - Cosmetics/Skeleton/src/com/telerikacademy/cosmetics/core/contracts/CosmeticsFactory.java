package com.telerikacademy.cosmetics.core.contracts;

import com.telerikacademy.cosmetics.models.Category;
import com.telerikacademy.cosmetics.models.cart.ShoppingCart;
import com.telerikacademy.cosmetics.models.products.Product;

public interface CosmeticsFactory {
    
    public Category createCategory(String name);
    
    public Product createProduct(String name, String brand, double price, String gender);
    
    public ShoppingCart createShoppingCart();
    
}