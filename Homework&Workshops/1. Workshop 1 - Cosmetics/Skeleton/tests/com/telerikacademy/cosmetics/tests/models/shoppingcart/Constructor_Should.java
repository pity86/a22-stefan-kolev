package com.telerikacademy.cosmetics.tests.models.shoppingcart;

import com.telerikacademy.cosmetics.models.cart.ShoppingCart;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Constructor_Should {
    
    @Test
    public void InitializeNewListOfProductsWhenCategoryIsCreated() {
        // Arrange, Act
        ShoppingCart cart = new ShoppingCart();
        
        // Assert
        Assertions.assertNotNull(cart.getProductList());
    }
    
}
