Travel site bugs


Title: About us and How to book links do not work

Summary About us and How to book links don't work. When you click on the link the expected window is not opened.

Steps to reproduce
1.	Go to: http:/www.phptravels.net/ru
2.	Go to the bottom section
3.	Press "About us"/"How to book” link

Expected result: About us and How to book pages are displayed or are inactive.

Actual result: About us and How to book pages are not displayed and no info is shown.

Priority: High

Additional info: Chrome browser used

Classification: version 1.0.1.2



Title: German version of site does not work

Summary German version of site does not work. When you select German the site is still in English.
Steps to reproduce
1.	Go to: http:/www.phptravels.net/ru
2.	Go to the upper section
3.	Select German from the language bar

Expected result: Website shows version in German language.

Actual result: Website shows version in English language.

Priority: Medium

Additional info: Chrome browser used

Classification: version 1.0.1.2



Title: Currency bar doesn't work properly

Summary Currency bar doesn't work properly. When you select TRY from the bar, it shows TURKISH.

Steps to reproduce
1.	Go to: http:/www.phptravels.net/ru
2.	Go to the bottom section
3.	Select TRY from currency bar

Expected result: Currency bar shows TRY.

Actual result: Currency bar shows TURKISH.

Priority: Medium

Additional info: Chrome browser used

Classification: version 1.0.1.2

