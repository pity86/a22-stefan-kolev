##### TAXI #####

test cases about Best Taxi app

TEST CASES

# TC01
Test Title: Order taxi when user is not registered

Priority: Very High

Precondition: 
Application is open
User is not registered

Description:
1. In order to serve client
2. As a taxi operator
3. I want to allow client to order taxi as registered user

Steps to execute:
1. Do not write any name and password in the field
2. Press "OK" button

Expected result: 
1.  "Please register first in order to use our service" message appears on screen
-----------------


# TC02
Test Title: Pay taxi with card

Priority: High

Precondition: 
Application is open
User is registered 

Description:
1. In order to serve client
2. As a taxi operator
3. I want to allow client to order taxi and pay with card

Steps to execute:
1. Select your location and destination (data: current location - 10 vasko str, destination: 2Short str)
2. Press "OK" button
3. Select type of payment (data: debit card)
4. Press "OK" button
5. Enter card type (data: Visa)
6. Enter card number (data: 1234567812345678)
7. Enter card name (Tom Joe)
8. Enter expiry date (23/12/23)
9. Enter cvv (777)
10. Press "OK" button

Expected result: 
1.  "Thank you for using our service. Soon will send you confirmation with information for the car" message appears on screen
-----------------


# TC03
Test Title: Cancel taxi after already confirmed

Priority: Medium

Precondition: 
User already ordered taxi and get confirmation
Application is open
User is registered 

Description:
1. In order to serve client
2. As a taxi operator
3. I want to allow client to cancel taxi ordered

Steps to execute:
1. Select "Cancel" 
2. Press "Confirm" button

Expected result: 
1.  "Your order is cancelled" message appears on screen
-----------------


# TC04
Test Title: Driver checks location of client

Priority: High

Precondition: 
Client already ordered taxi and get confirmation
Application is open
User (driver) is registered 

Description:
1. In order to serve user
2. As a taxi operator
3. I want to allow user to check client location

Steps to execute:
1. Select "Check order"
2. Select "Client location"

Expected result: 
1.  Red spot appears on a map on the screen
2.  Text "10vasko str" appears on the screen 
-----------------


# TC05
Test Title: Driver accepts request

Priority: High

Precondition: 
Client already ordered taxi
Application is open
User (driver) is registered 

Description:
1. In order to serve user
2. As a taxi operator
3. I want to allow user to accept client request

Steps to execute:
1. Select "New request"
2. Press "Accept" button

Expected result: 
1. "Request accepted" message appears on the screen
2. Confirmation message is sent to client
-----------------








