|Old Status| Action| Response| New State|
|----|----|----|-----|
|Initial|Create|Issue Created|TO DO|
|TO DO|Start Progress|Status Changed|IN PROGRESS|
|TO DO|Block Story|Status Changed|BLOCKED|
|TO DO|Complete Story|Status Changed|DONE|
|IN PROGRESS|Complete Story|Status Changed|DONE|
|IN PROGRESS|Block Story|Status Changed|BLOCKED|
|IN PROGRESS|Return to Backlog|Status Changed|TO DO|
|IN PROGRESS| Needs Review| Status Changed| IN REVIEW|
|BLOCKED| Unblock| Status Changed| IN PROGRESS|
|BLOCKED| Needs Review| Status Changed| IN REVIEW|
|BLOCKED| Return to Backlog|Status Changed|TO DO|
|BLOCKED| Complete Story|Status Changed|DONE|
|IN REVIEW| Block Story|Status Changed|BLOCKED|
|IN REVIEW| Return to Progress|Status Changed|IN PROGRESS|
|IN REVIEW| Complete Story|Status Changed|DONE|
|DONE| Return to Backlog|Status Changed|TO DO|