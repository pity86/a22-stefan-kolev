##### Homework ATM  #####

Tested on Reiffeisen bank ATM

Minimum withdrawal amount allowed 10 leva.
Withdrawal amount should be multiple of 10. 
Maximum withdrawal amount allowed per transaction 800 leva.
Maximum withdrawal amount allowed per day 1000 leva.

EVP TEST CASES

# TC01
Test Title: Withdraw - more than the account balance - negative

Priority: Very High

Precondition: User's card inserted in ATM and read, "Withdraw" selected, correct PIN entered
User's account balance is 490 leva.

Narrative:
1. In order to prevent misuse
2. As a bank
3. I want to disallow clients to withdraw amount ecceeding their current balance

Steps to execute:
1. Select "Other amount"
2. Write "500" in the field "Enter amount"
3. Press "OK"

Expected result: 
1. No money withdrawn. 
2. Message "Not enough money in account" appears on screen.
-----------------

# TC02
Test Title: Withdraw - less than the account balance

Priority: Very High

Precondition: User's card inserted in ATM and read, "Withdraw" selected, correct PIN entered
User's account balance is 490 leva.

Narrative:
1. In order to serve client
2. As a bank
3. I want to allow clients to withdraw amount less than their current balance

Steps to execute:
1. Select "Other amount"
2. Write "200" in the field "Enter amount"
3. Press "OK"

Expected result: 
1. 200 leva withdraw
-----------------



BVA TEST CASES

# TC03
Test Title: Withdraw - maximum withdrawal amount allowed per transaction in a single transaction

Priority: Very High

Precondition: User's card inserted in ATM and read, "Withdraw" selected, correct PIN entered
User's account balance is 1200 leva.

Narrative:
1. In order to follow bank's rule
2. As a bank
3. I want to allow clients to withdraw amount up to 800 leva in a single transaction

Steps to execute:
1. Select "Other amount"
2. Write "800" in the field "Enter amount"
3. Press "OK"

Expected result: 
1. 800 leva withdraw
-----------------

# TC04
Test Title: Withdraw - more than maximum withdrawal amount allowed per transaction in a single transaction - negative

Priority: Very High

Precondition: User's card inserted in ATM and read, "Withdraw" selected, correct PIN entered
User's account balance is 1200 leva.

Narrative:
1. In order to follow bank's rule
2. As a bank
3. I want to disallow clients to withdraw amount more than 800 leva in a single transaction

Steps to execute:
1. Select "Other amount"
2. Write "810" in the field "Enter amount"
3. Press "OK"

Expected result: 
1. Money does not withdraw. 
2. Message "Not possible to withdraw more than 800 leva in a single transaction" appears on screen.
-----------------

# TC05
Test Title: Withdraw - minimum withdrawal amount allowed in a single transaction

Priority: Very High

Precondition: User's card inserted in ATM and read, "Withdraw" selected, correct PIN entered
User's account balance is 1200 leva.

Narrative:
1. In order to follow bank's rule
2. As a bank
3. I want to allow clients to withdraw amount at least 10 leva in a single transaction

Steps to execute:
1. Select "10"
2. Press "OK"

Expected result: 
1. 10 leva withdraw
-----------------

# TC06
Test Title: Withdraw - less than minimum withdrawal amount allowed in a single transaction - negative

Priority: Very High

Precondition: User's card inserted in ATM and read, "Withdraw" selected, correct PIN entered
User's account balance is 1200 leva.

Narrative:
1. In order to follow bank's rule
2. As a bank
3. I want to disallow clients to withdraw amount less than 10 leva in a single transaction

Steps to execute:
1. Select "Other amount"
2. Write "9" in the field "Enter amount"
3. Press "OK"

Expected result: 
1. Money does not withdraw. 
2. Message "Not possible to withdraw less than 10 leva in a single transaction" appears on screen.
-----------------

# TC07
Test Title: Withdraw - maximum withdrawal amount allowed per day

Priority: Very High

Precondition: User's card inserted in ATM and read, "Withdraw" selected, correct PIN entered
User's account balance is 1200 leva. 
User already withdrawn 800 leva the same day

Narrative:
1. In order to follow bank's rule
2. As a bank
3. I want to allow clients to withdraw amount up to 1000 leva per day

Steps to execute:
1. Select "Other amount"
2. Write "200" in the field "Enter amount"
3. Press "OK"

Expected result: 
1. 200 leva withdraw
-----------------

# TC08
Test Title: Withdraw -  more than maximum withdrawal amount allowed per day - negative

Priority: Very High

Precondition: User's card inserted in ATM and read, "Withdraw" selected, correct PIN entered
User's account balance is 1200 leva, 
User already withdrawn 800 leva the same day

Narrative:
1. In order to follow bank's rule
2. As a bank
3. I want to allow clients to withdraw amount up to 1000 leva per day

Steps to execute:
1. Select "Other amount"
2. Write "210" in the field "Enter amount"
3. Press "OK"

Expected result: 
1. Money does not withdraw. 
2. Message "Not possible to withdraw more than 1000 leva per day" appears on screen.
-----------------



PIN TEST CASES

# TC09
Test Title: PIN - Enter Correct PIN

Priority: Very High

Precondition: Card inserted in ATM and read, "Withdraw" selected
Correct PIN is 7777.

Narrative:

1. In order to follow bank's rule
2. As a bank
3. I want to allow clients to use ATM service when correct PIN entered

Steps to execute:
1. Enter PIN "7777"
2. Select green button "Enter"

Expected result: 
1. "Choose amount to withdraw" message on screen appears.
-----------------

# TC10
Test Title: PIN - Enter Wrong PIN - negative

Priority: Very High

Precondition: Card inserted in ATM and read, "Withdraw" selected
Correct PIN is 7777.

Narrative:
1. In order to follow bank's rule
2. As a bank
3. I want to disallow clients to use ATM service when incorrect PIN entered

Steps to execute:
1. Enter PIN "7770"
2. Select green button "Enter"

Expected result: 
1. "Wrong PIN. Please enter again." message on screen appears.
-----------------

# TC11
Test Title: PIN - Enter Wrong PIN three times in a row - negative

Priority: Very High

Precondition: Card inserted in ATM and read, "Withdraw" selected 
Already entered two times wrong PIN 7770
Correct PIN is 7777

Narrative:
1. In order to follow bank's rule
2. As a bank
3. I want to block card when incorrect PIN entered three consecutive times at ATM

Steps to execute:
1. Enter PIN "7770"
2. Select green button "Enter"

Expected result: 
1. "Wrong PIN. No more tries. Your card is blocked. Please, contact bank support." message on screen appears.
-----------------



USE TEST CASES

# TC12
Test Title: Check account balance

Priority: Very High

Precondition: 
ATM screen is working
User's PIN is 7777,
User's account balance is 490 leva

Narrative:
1. In order to inform client
2. As a bank
3. I want to allow client to check their account balance

Steps to execute:
1. Insert card
   Language menu appears and "Select language" message
2. Select "English"
   Main Menu appears and message "Choose service"
3. Select "Check balance"
   "Please enter your PIN and press Enter" message appears on the screen
4. Write PIN "7777" and press Enter

Expected result: 
1.  "Your account balance is 490 leva" message appears on the screen
-----------------


# TC13
Test Title: Deposit money in ATM - negative

Priority: Very High

Precondition: 
ATM screen is working
User's PIN is 7777,
User's account balance is 400 leva
One 20 leva banknote is old and not possible to be count by the ATM

Narrative:
1. In order to satisfy client's needs
2. As a bank
3. I want to allow client to deposit money to their account balance at ATM

Steps to execute:
1. Insert card
   Language menu appears and "Select language" message
2. Select "English"
   Main Menu appears and message "Choose service"
3. Select "Other services"
   Other services menu appears and message "Choose service"
4. Choose "Deposit money"
   "Please enter your PIN and press Enter" message appears on the screen
5. Write PIN "7777" and press Enter
   Deposit money menu appears and message "Your current account balance is 400 leva. Choose amount to deposit"
6. Select "Other"
   "Please enter amount and press Enter" message appears on the screen
7. Enter "300" and press Enter
   "Please insert 300 leva in the ATM and press Enter" message appears
8. Insert 300 leva in the ATM and press Enter


Expected result: 
1. "Inserted amount is 280 leva. Deposit is rejected. Please try again" message appears on the screen
2. 300 leva return by the ATM
-----------------