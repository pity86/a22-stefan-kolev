## Decision tables 


|  Conditions | 1  |  2 |  3 |  4 | 5  | 6  | 7  | 8  |
|---|---|---|---|---|---|---|---|---|
|  Card inserted and read |  Y |  Y | Y  | Y| N  |  N |  N | N  |
|  PIN entered is correct |  Y |  Y |  N |  N | Y  | Y  | N  | N  |
|  Requested amount is available in card account | Y  | N  | Y  | N  | Y |  N |  Y | N  |
| <b>Actions</b>  |   |   |   |   |   |   |   |   |
| Funds are withdrawn  |  Y |  N | N  | N  | N  | N  | N  | N  |

## Real combinations 


|  Conditions | 1  |  2 |  3 |  4 |
|---|---|---|---|---|---|---|---|---|
|  Card inserted and read |  Y |  Y | Y  | N| 
|  PIN entered is correct |  Y |  Y | N |  ~ | 
|  Requested amount is available in card account | Y  | N  | ~  | ~  |
| <b>Actions</b>  |   |   |   |   |   |   |   |   |
| Funds are withdrawn  |  Y |  N | N  | N  |

## Real combinations 


|  Conditions | 1  |  2 |  3 |  4 |
|---|---|---|---|---|---|---|---|---|
|  Card inserted and read |  Y |  Y | Y  | N| 
|  PIN entered is correct |  Y |  Y | N |  N | 
|  Requested amount is available in card account | Y  | N  | N  | N  |
| <b>Actions</b>  |   |   |   |   |   |   |   |   |
| Funds are withdrawn  |  Y |  N | N  | N  |