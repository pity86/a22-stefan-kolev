package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.CategoryImpl;
import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class ProductBase implements Product {

    private static final int MINIMAL_NAME = 3;
    private static final int MAXIMAL_NAME = 10;
    private static final int MINIMAL_BRAND = 2;
    private static final int MAXIMAL_BRAND = 10;

    private String name;
    private String brand;
    private double price;
    private GenderType gender;

    private void setName(String name) {
        this.name = name;
    }

    private void setBrand(String brand) {
        this.brand = brand;
    }

    private void setPrice(double price) {
        this.price = price;
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getBrand() {
        return brand;
    }

    @Override
    public double getPrice() {
        return price;
    }

    @Override
    public GenderType getGender() {
        return gender;
    }

    /*@Override
    public String print() {

        System.out.println("Name is " + name );
        return null;
    }*/
    //Finish the class
    //implement proper interface (see contracts package)
    //validate

    ProductBase(String name, String brand, double price, GenderType gender) {
        if (name == null || brand == null || price < 0 || gender == null) {
            throw new IllegalArgumentException();
        }

        if (name.length() < MINIMAL_NAME || name.length() > MAXIMAL_NAME) {
            throw new IllegalArgumentException("Product name is out of range");

        }
        if (brand.length() < MINIMAL_BRAND || brand.length() > MAXIMAL_BRAND) {
            throw new IllegalArgumentException("Brand name is out of range");
        }

        setName(name);
        setBrand(brand);
        setPrice(price);
        this.gender = gender;

        //this.gender = gender;
    }

    public String print() {
//        return " # " + getName() + " " + getBrand() + " #Price " + getPrice() + " #Gender " + getGender();

        return String.format("#%s %s%n #Price: $%.2f%n #Gender: %s", getName(), getBrand(), getPrice(), getGender());
    }
}

