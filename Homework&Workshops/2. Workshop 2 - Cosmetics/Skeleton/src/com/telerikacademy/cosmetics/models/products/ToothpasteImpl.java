package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Toothpaste;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

public class ToothpasteImpl extends ProductBase implements Toothpaste {

    private List<String> ingredients;

    public ToothpasteImpl(String name, String brand, double price, GenderType gender, List<String> ingredients) {

        super(name, brand, price, gender);
        setIngredients(ingredients);

//        throw new NotImplementedException();

    }

    @Override
    public List<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<String> ingredients) {
        if (ingredients == null) {
            throw new IllegalArgumentException();
        }
        this.ingredients = ingredients;
    }

    public String print() {
        return String.format("%s%n #Ingredients: %s%n ===",super.print(), ingredients.toString());
    }
}
