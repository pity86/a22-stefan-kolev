package com.telerikacademy.cosmetics.models;


import com.telerikacademy.cosmetics.models.contracts.Category;
import com.telerikacademy.cosmetics.models.contracts.Product;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

public class CategoryImpl implements Category {
    //use constants for validations values


    private String name;
    private List<Product> products;


    public CategoryImpl(String name) {
        //validate
        //initialize the collection
        if (name == null) {
            throw new IllegalArgumentException();
        }
        products = new ArrayList<>();  // Initialize
        this.name = name;

    }

    public String getName() {
        return name;
    }

    public List<Product> getProducts() {
        //todo why are we returning a copy? Replace this comment with explanation! // because it is reference type.

        return new ArrayList<>(products);
    }

    public void addProduct(Product product) {

        if (product == null) {
            throw new IllegalArgumentException();
        }
        products.add(product);
    }

    public void removeProduct(Product product) {

        if (product == null) {
            //validate
            throw new IllegalArgumentException();
        }
        products.remove(product);
    }

    //The engine calls this method to print your category! You should not rename it!
    public String print() {
        if (products.size() == 0) {
            return String.format("#Category: %s\n" +
                    " #No product in this category", name);
        }

        //finish ProductBase class before implementing this method
        //   else {

//        StringBuilder result = new StringBuilder();
//
//        result.append(String.format ("#Category: %s\n", name));
//
//        for (Product product : products) {
//            result.append(product.print());
////            .append("\n ===");   може без този ред също
//        }
//
//        return result.toString();
//
//    }
//}

            String message = String.format ("#Category: %s\n", name);

            for (Product product: products){
                 message += product.print();
            }
            return message;
        }
//        throw new NotImplementedException();
    }
    

