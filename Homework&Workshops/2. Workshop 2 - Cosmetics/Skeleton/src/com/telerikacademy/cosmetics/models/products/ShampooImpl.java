package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Product;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

public class ShampooImpl extends ProductBase implements Shampoo {

    private int milliliters;
    private UsageType usage;

    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType usage) {

        super(name, brand, price, gender);
        setMilliliters(milliliters);
        this.usage = usage;

//        throw new NotImplementedException();
        if (milliliters < 0 || usage==null) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public int getMilliliters() {
        return milliliters;
    }

    @Override
    public UsageType getUsage() {
        return usage;
    }

    public void setMilliliters(int milliliters) {
        this.milliliters = milliliters;
    }


    public String print() {
//        return super.print() + " #Milliliters " + milliliters + " #Usage " + everyDay.toString() + " ===";

        return String.format("%s%n #Milliliters: %d%n #Usage: %s%n ===", super.print(), milliliters, usage.toString());
    }
}
