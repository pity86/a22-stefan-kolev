package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.ScentType;
import com.telerikacademy.cosmetics.models.contracts.Cream;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class CreamImpl extends ProductBase implements Cream {
    private ScentType scent;

    public CreamImpl(String name, String brand, double price, GenderType gender, ScentType scent) {
        super(name, brand, price, gender);
        setScent(scent);
        //throw new NotImplementedException();

        if (scent == null) {
            throw new IllegalArgumentException();
        }
    }

    public ScentType getScent(){
        return scent;
    }

    public void setScent(ScentType scent) {
        this.scent = scent;
    }

    public String print(){
//        return super.print() + " #Scent " + scent.toString() + " ===";

        return String.format("%s%n #Scent: %s%n ===", super.print(), scent.toString());

        //getScent() or setScent() or scent.toString()
    }

}
