For successfull execution of the tests you will need the following : 

1. IntelliJ - download and install from https://www.jetbrains.com/idea/download/
2. Clone Git Repository https://gitlab.com/pity86/avalontelerik/
3. Run IntelliJ and open finalProject
4. Open config.properties and select browser
4. Open testCases package
5. Open and Run PublicTest Class
6. Open and Run PrivateTest Class
7. Open and Run ChangePasswordTest Class
8. Open and Run AdminTest Class
9. Open and Run Runner Class (for BDD tests)
