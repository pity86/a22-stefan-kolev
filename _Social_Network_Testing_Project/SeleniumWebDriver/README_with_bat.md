For successfull execution of the tests you will need the following : 

1. IntelliJ - download and install from https://www.jetbrains.com/idea/download/
2. Apache Maven - download and install from https://maven.apache.org/download.cgi
3. Clone Git Repository https://gitlab.com/pity86/avalontelerik/
4. Run IntelliJ and open finalProject
5. Open config.properties and select browser
6. Run RunPublicTest bat file
7. Run RunPrivateTest bat file
8. Run RunChangePasswordTest bat file
9. Run RunAdminTest bat file
10. Run RunRunner bat file (for BDD tests)
