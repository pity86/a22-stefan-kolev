package testCases;

import com.telerikacademy.finalproject.pages.*;
import com.telerikacademy.finalproject.utils.Constants;
import com.telerikacademy.finalproject.utils.UserActions;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AdminTest extends BaseTest {

    NavigationPage navPage;
    LoginPage login = new LoginPage();
    UsersPage usersPage;

    private NationalitiesAddEditPage nationalitiesAddEditPage = new NationalitiesAddEditPage();
    private CategoriesAddEditPage categoriesAddEditPage = new CategoriesAddEditPage();
    private PostOverviewPage postOverviewPage = new PostOverviewPage();
    private PostCreateEditPage postCreateEditPage = new PostCreateEditPage();
    private ProfileOverviewPage profileOverviewPage = new ProfileOverviewPage();

    @Before
    public void loginAdmin() {
        login = new NavigationPage().openLoginPageAfterSignIn();
        navPage = new LoginPage().login(Constants.ADMIN_USERNAME,Constants.ADMIN_PASSWORD);
    }

    @After
	public void closePage(){
		UserActions.quitDriver();
	}

    @Test
    public void EditExistingUserProfile() {
        usersPage = navPage.openUsersPage();
        profileOverviewPage.editOtherUserProfile();
    }

    @Test
    public void deleteOtherUserProfile() {
        usersPage = navPage.openUsersPage();
        profileOverviewPage.deleteUser();
    }

    @Test
    public void EditExistingPost() {
        navPage = navPage.openSettingsPosts();
        postCreateEditPage.editPost();
    }

    @Test
    public void DeleteExistingPost() {
        navPage = navPage.openSettingsPosts();
        postCreateEditPage.deletePost();
    }

    @Test
    public void EditExistingComment() {
        navPage = navPage.openSettingsPosts();
        postOverviewPage.editComment();
    }

    @Test
    public void DeleteExistingComment() {
        navPage = navPage.openSettingsPosts();
        postOverviewPage.deleteComment();
    }

    @Test
    public void AddNewCategory() {
        navPage = navPage.openSettingsCategories();
        categoriesAddEditPage.addCategory();
    }

    @Test
    public void EditExistingCategory() {
        navPage = navPage.openSettingsCategories();
        categoriesAddEditPage.editCategory();
    }

    @Test
    public void DeleteExistingCategory() {
        navPage = navPage.openSettingsCategories();
        categoriesAddEditPage.deleteCategory();
    }

    @Test
    public void AddNewNationality() {
        navPage = navPage.openSettingsNationalities();
        nationalitiesAddEditPage.addNationality();
    }

    @Test
    public void EditExistingNationality() {
        navPage = navPage.openSettingsNationalities();
        nationalitiesAddEditPage.editNationality();
    }

    @Test
    public void DeleteExistingNationality() {
        navPage = navPage.openSettingsNationalities();
        nationalitiesAddEditPage.deleteNationality();
    }
}

