package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Constants;
import com.telerikacademy.finalproject.utils.Utils;

public class ChangePasswordPage extends BasePage {

    CommonPage commonPage = new CommonPage();

    public final String homeButton = "login.HomePage";
    public final String changePasswordOldPass = "changePassword.OldPass";
    public final String changePasswordEnterPass = "changePassword.EnterPass";
    public final String changePasswordConfirmPass = "changePassword.ConfirmPass";
    public final String changePasswordWrongOldPasswordMessage = "changePassword.WrongOldPasswordMessage";

    public void fillChangePasswordFields() {
        Utils.LOG.info("Changing password fields filling");
        actions.typeValueInField(Constants.USER_THREE_PASSWORD, changePasswordOldPass);
        actions.typeValueInField(Constants.USER_THREE_UPDATED_PASSWORD, changePasswordEnterPass);
        actions.typeValueInField(Constants.USER_THREE_UPDATED_PASSWORD, changePasswordConfirmPass);
        actions.clickElement(commonPage.saveButton);
        actions.implicitWait(Constants.SHORT_WAIT);
    }

    public void fillChangePasswordFieldsWrongData() {
        Utils.LOG.info("Changing password fields filling with wrong data");
        actions.typeValueInField(actions.createRandomString(2), changePasswordOldPass);
        actions.typeValueInField(Constants.USER_UPDATED_PASSWORD, changePasswordEnterPass);
        actions.typeValueInField(Constants.USER_UPDATED_PASSWORD, changePasswordConfirmPass);
        actions.clickElement(commonPage.saveButton);
        actions.implicitWait(Constants.SHORT_WAIT);
    }

    // STORIES
    public void returnHomePage(){
        Utils.LOG.info("Return to home page");
        actions.clickElement(homeButton);
        actions.implicitWait(Constants.SHORT_WAIT);
    }
}
