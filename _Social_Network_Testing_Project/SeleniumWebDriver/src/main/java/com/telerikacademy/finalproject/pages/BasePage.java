package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import static java.lang.Integer.parseInt;

public abstract class BasePage {

    protected String url;
    protected WebDriver driver;
    public UserActions actions;


    public BasePage(String urlKey) {
        String pageUrl = Utils.getConfigPropertyByKey(urlKey);
        this.driver = Utils.getWebDriver();
        this.url = pageUrl;
        actions = new UserActions();
    }
    public BasePage (){
        this.driver = Utils.getWebDriver();
        actions = new UserActions();
    }
}
