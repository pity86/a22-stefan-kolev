package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Constants;

public class PostsPage extends BasePage {

    public final String latestPostsMoreButton = "latestPosts.MoreButton";

    public PostOverviewPage openPostOverviewPage(String postNameKey) {
        actions.clickElement(postNameKey);
        actions.implicitWait(Constants.SHORT_WAIT);
        return new PostOverviewPage();
    }
}
