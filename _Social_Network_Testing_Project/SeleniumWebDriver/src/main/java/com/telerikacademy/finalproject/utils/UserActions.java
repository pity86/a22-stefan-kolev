package com.telerikacademy.finalproject.utils;

import com.telerikacademy.finalproject.pages.ProfileRegistrationEditPage;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserActions {

    final WebDriver driver;
    public ProfileRegistrationEditPage profileRegistrationEditPage;

    public WebDriver getDriver() {
        return driver;
    }

    public UserActions() {
        this.driver = Utils.getWebDriver();
    }

    public static void loadBrowser(String baseUrlKey) {
        Utils.LOG.info("loading browser");
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey(baseUrlKey));
    }

    public static void quitDriver() {
        Utils.LOG.info("Quitting driver");
        Utils.tearDownWebDriver();
    }

    //############# ELEMENT OPERATIONS #########

    public void clickElement(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.click();
    }

    public void clearField(String field, Object... fieldArguments) {
        Utils.LOG.info("Clearing field data");
        String locator = Utils.getUIMappingByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.clear();
    }

    public void typeValueInField(String value, String field, Object... fieldArguments) {
        Utils.LOG.info("Typing value in field");
        String locator = Utils.getUIMappingByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.sendKeys(value);
    }

    public void hoverElement(String key, Object... arguments) {
        Utils.LOG.info("Hovering on element");
        Actions action = new Actions(driver);
        String locator = Utils.getUIMappingByKey(key, arguments);
        WebElement element = driver.findElement(By.xpath(locator));
        action.moveToElement(element).perform();
    }

    public void uploadImage(String fileLocation, String key, Object... arguments) {
        Utils.LOG.info("Uploading image");
        String locator = Utils.getUIMappingByKey(key, arguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.sendKeys(fileLocation);
    }

    public String createRandomString(int n) {
        Utils.LOG.info("Creating random string");
        return RandomStringUtils.randomAlphabetic(n);
    }

    //############# WAITS #########

    public boolean isElementPresentUntilTimeout(String locator, int timeout, Object... arguments) {
        Utils.LOG.info("Waiting for element");
        try {
            String xpath = Utils.getUIMappingByKey(locator, arguments);
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(xpath)));
            return true;
        } catch (Exception exception) {
            return false;
        }
    }

    public void implicitWait(int seconds) {
        Utils.LOG.info("Waiting");
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }

    //############# ASSERTS #########

    public void assertElementPresent(String locator) {
        Utils.LOG.info("Asserting element is present");
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    public boolean elementIsPresentOnPage(String locator) {
        Utils.LOG.info("Asserting element is present");
        return !driver.findElements(By.xpath(Utils.getUIMappingByKey(locator))).isEmpty();
    }
}
