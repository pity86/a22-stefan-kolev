package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Constants;
import com.telerikacademy.finalproject.utils.Utils;

public class CommonPage extends BasePage {

    public final String searchKeyWord = "search.KeyWord";
    public final String searchButton = "search.Button";

    public final String saveButton = "save.Button";

    public final String regularUserTwo = "veselinMinev1";
    public final String regularUserOne = "veselinMinev";
    public final String titlePostOne = "titlePostOne";

    public final String publicPostAdmin = "publicPostAdmin";

    public final String profileOverviewEmail = "profileOverview.Email";
    public final String profileOverviewGender = "profileOverview.Gender";
    public final String profileOverviewNationality = "profileOverview.Nationality";
    public final String emailVeselinMinev = "email.VeselinMinev";
    public final String genderVeselinMinev = "gender.VeselinMinev";
    public final String nationalityVeselinMinev = "nationality.VeselinMinev";

    public CommonPage searchValue(String value) {
        Utils.LOG.info("Searching for element");
        actions.typeValueInField(value, searchKeyWord);
        actions.clickElement(searchButton);
        actions.implicitWait(Constants.MEDIUM_WAIT);
        return new CommonPage();
    }
}
