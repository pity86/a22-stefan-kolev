==============================================================================================================================
Automated tests are in white colour

Public Part

OOO ##1 Registration - Check with valid data - only required fields filled in - Prio 1
OOO ##2 Registration - Check with invalid data - only required fields filled in - negative - Prio 1
## 3 Registration - Check with empty Username - negative - Prio 2
## 4 Latest Posts - Check chronological order of public posts - Prio 2
OOO ##5 Public User Profiles - Check user information - Prio 1
## 6 Public User Profiles - Check user posts order - Prio 1


OOO ##7 Login - Check with valid credentials - Prio 1
OOO ##8 Login - Check with invalid credentials all fields - negative - Prio 1
## 9 Login - Check with invalid username - negative - Prio 1
## 10 Login - Check with invalid password - negative - Prio 1
OOO ##11 Forgot Password - Check with valid email - Prio 1
OOO ##12 Forgot Password - Check with invalid email - negative - Prio 1

Private Part

## 13 Private Home Page - Check "LOG OUT" button navigation - Prio 1

## 14 Profile Administration - Check user information - Prio 4

## 15 Edit Profile - Check changing all fields with valid data - Prio 2
## 16 Edit Profile - Check changing all fileds with invalid data - Prio 2

OOO ##17 Change Password - Check with valid data - Prio 1
OOO ##18 Change Password - Check with invalid old password - negative - Prio 1
## 19 Change Password - Check with less than 8 characters Password - negative - Prio 1
## 20 Change Password - Check with more than 40 characters Password - negative - Prio 1

OOO ##21 User Interaction - Check Connection to regular user - Prio 1
## 22 User Interaction - Check "DISCONNECT" button navigation - Prio 2

OOO ##23 User Requests - Check confirmation - Prio 1
## 24 User Requests - Check rejection - Prio 2

OOO ##25 Create Post - Check public post creation - Prio 1
## 26 Create Post - Check connections post creation - Prio 1

OOO ##27 Comment Post - Check commenting a post created by other user - Prio 2
## 28 Comment Post - Check if newest comments are shown along with the post - Prio 3
## 29 Comment Post - Check if "See all" button expands Comments section - Prio 2
## 30 Like Post - Like a post created by other user - Prio 2
## 31 Dislike Post - Check Dislike a post created by other user - Prio 2

OOO ##32 Public User Profiles - Check Search with a keyword partially matching several existing User Names - Prio 4
## 33 Latest Posts - Check Search with a keyword partially matching several existing public Post Names - Prio 4
## 34 My Friends Posts - Check Sort by Date - Prio 3
## 35 My Friends Posts - Check Sort by Likes - Prio 3
## 36 My Friends Posts - Check Sort by Comments - Prio 3
## 37 My Friends Posts - Check Category filter - Prio 3
## 38 My Friends Post - Check chronological order of my connections posts - Prio 1

Admin Part

OOO ##39 Admin Edit Other User Profile - Check changing all fields with valid data - Prio 1
OOO ##40 Admin Delete User Profile - Check deletion user profile - Prio 1
OOO ##41 Admin Edit Post - Check editing of post - Prio 1
OOO ##42 Admin Delete Post - Check deletion of a post created by other user - Prio 1
OOO ##43 Admin Edit comment - Check changing comment created by other user - Prio 1
OOO ##44 Admin Delete comment - Check deleting comment created by other user - Prio 1
OOO ##45 Admin Settings - Add category with valid data - Prio 2
## 46 Admin Settings - Add category with invalid data - negative Prio 3
OOO ##47 Admin Settings - Edit category with valid data - Prio 3
## 48 Admin Settings - Edit category with invalid data - negative - Prio 3
## 49 Admin Settings - Delete category - Prio 2
OOO ##50 Admin Settings - Add Nationality with valid data - Prio 2
## 51 Admin Settings - Add Nationality with invalid data - negative - Prio 3
OOO ##52 Admin Settings - Edit Nationality with valid data - Prio 2
## 53 Admin Settings - Edit Nationality with invalid data - negative - Prio 3
OOO ##54 Admin Settings - Delete Nationality - Prio 2
## 55 Admin Settings Posts - Check Search with a keyword partially matching one connections non friend existing Post Name - Prio 3