##### Healthy Food #####

Test cases about Healthy Food website

https://sheltered-badlands-59110.herokuapp.com/

### Prority ###

Prio 1, Prio 2, Prio 3, Prio 4

## Public part

Home Page - Check "HOME" button navigation
Home Page - Check "LATEST POSTS" button navigation
Home Page - Check "USERS" button navigation
Home Page - Check "SIGN IN" button navigation
Home Page - Check "SIGN UP" button navigation
Home Page - Check "ABOUT US" button navigation

Browser - Check browser back navigation
Browser - Check browser forward navigation

About Us Page - Check Silvia's card navigation on Linkedin
About Us Page - Check Tanya's card navigation on Linkedin
About Us Page - Check Video

Registration - Check with valid data - all fields filled in
Registration - Check with valid data - only required fields filled in

Registration - Check with empty all fields - negative

Registration - Check with empty Username - negative
Registration - Check with invalid Username - negative
Registration - Check with existing Username - negative

Registration - Check with different Password and Confirm Password - negative (Password does't match!)

Registration - Check with empty Password - negative
Registration - Check with less than 8 characters Password - negative
Registration - Check with more than 40 characters Password - negative
Registration - Check with no lower case letter Password - negative
Registration - Check with no upper case letter Password - negative
Registration - Check with no numeric character Password - negative
Registration - Check with no special character Password - negative

Registration - Check with less than 2 characters First name - negative
Registration - Check with more than 50 characters First name - negative

Registration - Check with less than 2 characters Last name - negative
Registration - Check with more than 50 characters Last name - negative

Registration - Check with no Visibility's type selected - negative

Registration - Check with no Profile picture selected - negative

Registration - Check "Home Page" button navigation - negative
Registration - Check "Login" button navigation - negative

Login - Check with valid credentials
Login - Check with empty credentials all fields - negative
Login - Check with invalid credentials all fields - negative
Login - Check with empty username field- negative
Login - Check with invalid username - negative
Login - Check with empty password field - negative
Login - Check with invalid password - negative

Login - Check "Home Page" button navigation
Login - Check "Registration" button navigation
Login - Check "Forgot Password" button navigation

Forgot Password - Check with valid email
Forgot Password - Check with empty email field - negative
Forgot Password - Check with invalid email - negative

Forgot Password - Check "Home Page" button navigation
Forgot Password - Check "Login" button navigation
Forgot Password - Check "Registration" button navigation

Public User Profiles - Check user information
Public User Profiles - Check user posts order
Public User Profiles - Check Search with a keyword exact matching existing User Name
Public User Profiles - Check Search with a keyword partially matching one existing User Name
Public User Profiles - Check Search with a keyword partially matching several existing User Names
Public User Profiles - Check Search with a keyword no matching existing User Names
Public User Profiles - Check Search with no keyword - negative

Latest Posts - Check chronological order of public posts - High                

Latest Posts - Check Search with a keyword exact matching existing public Post Name
Latest Posts - Check Search with a keyword exact matching existing connections Post Name - negative
Latest Posts - Check Search with a keyword partially matching one public existing Post Name
Latest Posts - Check Search with a keyword partially matching one connections existing Post Name - negative
Latest Posts - Check Search with a keyword partially matching several existing public Post Names
Latest Posts - Check Search with a keyword partially matching several existing connections Post Names - negative
Latest Posts - Check Search with a keyword no matching any existing public Post Names part - negative
Latest Posts - Check Search with a keyword no matching any existing connections Post Names part - negative
Latest Posts - Check Search with no keyword - negative
Latest Posts - Check Sort by Date
Latest Posts - Check Sort by Likes
Latest Posts - Check Sort by Comments
Latest Posts - Check Category filter

## Private part

Private Home Page - Check "HOME" button navigation
Private Home Page - Check "ABOUT US" button navigation
Private Home Page - Check "LOG OUT" button navigation
Private Home Page - Check "USERS > USERS" drop-down navigation
Private Home Page - Check "USERS > REQUESTS" drop-down navigation
Private Home Page - Check "USERS > FRIENDS" drop-down navigation
Private Home Page - Check "LATEST POSTS > NEW POST" drop-down navigation
Private Home Page - Check "LATEST POSTS > MY FRIENDS POSTS" drop-down navigation
Private Home Page - Check "LATEST POSTS > ALL POSTS" drop-down navigation

Profile Administration - Check user information

Profile Administration - Check Lately posts - default view
Profile Administration - Check Lately posts - more

Edit Profile - Check changing all fields with valid data
Edit Profile - Check changing all fileds with invalid data
Edit Profile - Check changing Profile picture's visibility only
Edit Profile - Check changing profile picture file only
Edit Profile - Check changing First name only
Edit Profile - Check changing Last name only
Edit Profile - Check changing Nationality only
Edit Profile - Check changing Gender only
Edit Profile - Check changing Age only
Edit Profile - Check changing About only

Edit Profile - Check "CHANGE PASSWORD" button navigation

Change Password - Check "Home Page" button navigation
Change Password - Check "Login" button navigation
Change Password - Check "Registration" button navigation
Change Password - Check with valid data
Change Password - Check with empty all fields
Change Password - Check with invalid old password - negative
Change Password - Check with less than 8 characters Password - negative
Change Password - Check with no lower case letter Password - negative
Change Password - Check with no upper case letter Password - negative
Change Password - Check with no numeric character Password - negative
Change Password - Check with no special character Password - negative 

User Interaction - Check Connection to regular user 
User Interaction - Check Connection to admin user 
User Interaction - Check Connection to himself - negative 
User Interaction - Check Connection request rejection 
User Interaction - Check Connection Lately posts navigation 
User Interaction - Check "DISCONNECT" button navigation

User Requests - Check if yellow bell icon appears when user has a request
User Requests - Check confirmation
User Requests - Check rejection

Create Post - Check with valid data - all fields filled in 
Create Post - Check with valid data - only required fields field in 
Create Post - Check with empty all fields - negative
Create Post - Check with no Post visibility's type selected - negative
Create Post - Check with no Post picture or video selected - negative
Create Post - Check with empty Title field - negative
Create Post - Check with less than 2 characters Title - negative
Create Post - Check with more than 100 characters Title - negative
Create Post - Check with no Category selected
Create Post - Check with empty Description field
Create Post - Check public post creation
Create Post - Check connections post creation

Comment Post - Check if "See all" button expands Comments section - Prio 2

Comment Post - Check commenting a post created by the user himself
Comment Post - Check commenting a post created by other user

Edit comment - Check changing comment created by the user himslef
Delete comment - Check deleting comment created by the user himself

Like Comment - Like a comment created by the user himself
Like Comment - Like a comment created by the other user

Dislike Comment - Check Dislike a comment created by the user himself
Dislike Comment - Check Dislike a comment created by other user

Like Post - Like a post created by the user himself
Like Post - Like a post created by the other user

Dislike Post - Check Dislike a post created by the user himself
Dislike Post - Check Dislike a post created by other user

My Friends Posts - Check Search with a keyword exact matching existing friend Post Name
My Friends Posts - Check Search with a keyword exact matching existing non friend Post Name - negative
My Friends Posts - Check Search with a keyword partially matching one friend existing Post Name
My Friends Posts - Check Search with a keyword partially matching one non friend existing Post Name - negative
My Friends Posts - Check Search with a keyword partially matching several existing friend Post Names
My Friends Posts - Check Search with a keyword partially matching several existing non friend Post Names - negative
My Friends Posts - Check Search with a keyword no matching any existing friend Post Names part - negative
My Friends Posts - Check Search with a keyword no matching any existing non friend Post Names part - negative
My Friends Posts - Check Search with no keyword - negative
My Friends Posts - Check Sort by Date
My Friends Posts - Check Sort by Likes
My Friends Posts - Check Sort by Comments
My Friends Posts - Check Category filter
My Friends Posts - Check chronological order of my friends posts - Very High


## Admin part 

Admin Home Page - Check "LOG OUT" button navigation

Admin Edit Profile - Check changing all fields with valid data
Admin Edit Profile - Check changing Profile picture's visibility only
Admin Edit Profile - Check changing profile picture file only
Admin Edit Profile - Check changing First name only
Admin Edit Profile - Check changing Last name only
Admin Edit Profile - Check changing Nationality only
Admin Edit Profile - Check changing Gender only
Admin Edit Profile - Check changing Age only
Admin Edit Profile - Check changing About only

Admin Edit Other User Profile - Check changing all fields
Admin Edit Other User Profile - Check changing Profile picture's visibility only
Admin Edit Other User Profile - Check changing profile picture file only
Admin Edit Other User Profile - Check changing First name only
Admin Edit Other User Profile - Check changing Last name only
Admin Edit Other User Profile - Check changing Nationality only
Admin Edit Other User Profile - Check changing Gender only
Admin Edit Other User Profile - Check changing Age only
Admin Edit Other User Profile - Check changing About only

Admin Delete User Profile - Check deletion user profile
Admin Delete User Profile - Check deletion own admin profile

Admin Edit Post - Check changing visibility - from public to connections
Admin Edit Post - Check changing visibility - from connections to public
Admin Edit Post - Check changing Title only
Admin Edit Post - Check changing Description only
Admin Edit Post - Check changing Category only
Admin Edit Post - Check changing Picture with Picture only
Admin Edit Post - Check changing Picture with Video only
Admin Edit Post - Check changing Video with Picture only
Admin Edit Post - Check changing Video with Video only

Delete Post - Check deletion of a post created by the user himself
Delete Post - Check deletion of a post created by other user

Like Post - Like a post created by the user himself
Like Post - Like a post created by the other user

Dislike Post - Check Dislike a post created by the user himself
Dislike Post - Check Dislike a post created by other user

Comment Post - Check commenting a post created by the user himself
Comment Post - Check commenting a post created by other user

Edit comment - Check changing comment created by the user himslef
Edit comment - Check changing comment created by other user 

Delete comment - Check deleting comment created by the user himself
Delete comment - Check deleting comment created by other user

Like Comment - Like a comment created by the user himself
Like Comment - Like a comment created by the other user

Dislike Comment - Check Dislike a comment created by the user himself
Dislike Comment - Check Dislike a comment created by other user

Admin Settings - Check "Settings Categories" button navigation
Admin Settings - Add category with valid data
Admin Settings - Add category with invalid data
Admin Settings - Add category with no emoticon - negative
Admin Settings - Add category with empty Category's name field - negative
Admin Settings - Add category with less than 2 characters name - negative
Admin Settings - Add category with more than 50 characters name - negative
Admin Settings - Edit category with valid data
Admin Settings - Edit category with invalid data - negative
Admin Settings - Edit category with change of emoticon and name
Admin Settings - Edit category with change of emoticon
Admin Settings - Edit category with change of name
Admin Settings - Delete category

Admin Settings - Add Nationality with valid data
Admin Settings - Add Nationality with invalid data - negative
Admin Settings - Add Nationality with less than 2 characters name - negative
Admin Settings - Add Nationality with more than 50 characters name - negative
Admin Settings - Edit Nationality with valid data
Admin Settings - Edit Nationality with invalid data - negative
Admin Settings - Edit Nationality with less than 2 characters name - negative
Admin Settings - Edit Nationality with more than 50 characters name - negative
Admin Settings - Delete Nationality

Admin Settings Posts - Check Search with a keyword exact matching existing connections non friend Post Name 
Admin Settings Posts - Check Search with a keyword partially matching one connections non friend existing Post Name 
Admin Settings Posts - Check Search with a keyword partially matching several existing connections non friend Post Names 
Admin Settings Posts - Check Search with a keyword no matching any existing Post Names part 
Admin Settings Posts - Check Search with no keyword - negative
Admin Settings Posts - Check Sort by Date
Admin Settings Posts - Check Sort by Likes
Admin Settings Posts - Check Sort by Comments
Admin Settings Posts - Check Category filter



